package com.example.tasktimer;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Basis database class for the application
 * The AppProvider class is the one that uses this.
 * Singleton class
 */
class AppDatabase extends SQLiteOpenHelper {

    private static final String TAG = "AppDatabase";

    public static final String DATABASE_NAME = "TaskTimer.db";
    public static final int DATABASE_VERSION = 2;

    private static AppDatabase instance = null;

    private AppDatabase (Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d(TAG, "AppDatabase: Constructor");
    }//We only want a single instance of the class to be created

    /**
     * SQLite can support multiple users but it locks the entire database while writing to it. If we allow different initialisation of the class then each one will be
     * a different database user with their own connection to it.
     * Called automatically when the database is created for the first time. This is where the creation of tables and the initial population of the tables should happen.
     * @param sqLiteDatabase
     * Create database table
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d(TAG, "onCreate: starts");
        String ssql;
//        ssql = "CREATE TABLE Tasks(_id INTEGER PRIMARY KEY NOT NULL, Name TEXT NOT NULL, Description TEXT, SortOrder Integer, CategoryID INTEGER)";
        ssql = "CREATE TABLE "+TasksContract.TABLE_NAME +"("
                +TasksContract.Columns._ID+" INTEGER PRIMARY KEY NOT NULL, "
                +TasksContract.Columns.TASK_NAME+" TEXT NOT NULL, "
                +TasksContract.Columns.TASK_DESCRIPTION+" TEXT, "
                +TasksContract.Columns.TASK_SORTORDER+" INTEGER);";
        Log.d(TAG, "onCreate: "+ssql);
        sqLiteDatabase.execSQL(ssql);

        addTimingsTable(sqLiteDatabase);
    }

    /**
     * Get an instance of the AppDatabase class
     * @param context AppProviders context
     * @return
     */
    static AppDatabase getInstance(Context context){

        if (instance == null){
            Log.d(TAG, "getInstance: Creating instance");
            instance = new AppDatabase(context);
        }
        return instance;
    }

    //It is not thread safe

    /**
     * Called when the database needs to be upgraded. The implementation should use this method to drop tables, add tables, or do anything
     * else it needs to upgrade to the new schema version.
     * @param sqLiteDatabase
     * @param i
     * @param i1
     */

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Log.d(TAG, "onUpgrade: starts");
        switch (i){
            case 1:
                //upgrade logic from version 1
                addTimingsTable(sqLiteDatabase);
                break;
            default:
                throw new IllegalStateException("onUpgrade{} unkown new Version "+i1);
        }
    }
    /**
     * We have used a contract that defines the column name, so that any external programes that use our content provider knows which columns are available in the
     * database
     */

    public void addTimingsTable(SQLiteDatabase sqLiteDatabase){
        String ssql = "CREATE TABLE "+TimingsContract.TABLE_NAME+"("
                +TimingsContract.Columns._ID+" INTEGER PRIMARY KEY NOT NULL, "
                +TimingsContract.Columns.TIMING_TASK_ID+" INTEGER NOT NULL, "
                +TimingsContract.Columns.TIMING_START_TIME+" INTEGER, "
                +TimingsContract.Columns.TIMING_DURATION+" INTEGER);";

        sqLiteDatabase.execSQL(ssql);

        ssql = "CREATE TRIGGER Remove_TASK"
                +" AFTER DELETE ON "+TasksContract.TABLE_NAME
                +" FOR EACH ROW"
                +" BEGIN"
                +" DELETE FROM "+TimingsContract.TABLE_NAME
                +" WHERE "+TimingsContract.Columns.TIMING_TASK_ID+" = OLD."+TasksContract.Columns._ID+";"
                +" END;";

        sqLiteDatabase.execSQL(ssql);
    }

}
