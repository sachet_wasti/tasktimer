package com.example.tasktimer;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import java.net.URI;

/**
 * Provider of TaskTimer application. This is the only class that knows about {@link AppDatabase}
 * A content provider presents data to external applications as one or more tables that are similar
 * to the tables found in a relational database. A row represents an instance of some type of data the
 * provider collects, and each column in the row represents an individual piece of data collected for an instance.
 *
 * When you want to access data in a content provider, you use the ContentResolver object in your application's
 * Context to communicate with the provider as a client. The ContentResolver object communicates with the
 * provider object, an instance of a class that implements ContentProvider. The provider object receives data
 * requests from clients, performs the requested action, and returns the results. This object has methods that call
 * identically-named methods in the provider object, an instance of one of the concrete subclasses of ContentProvider.
 * The ContentResolver methods provide the basic "CRUD" (create, retrieve, update, and delete) functions of persistent storage.
 */
public class AppProvider extends ContentProvider {
    private static final String TAG = "AppProvider";

    private AppDatabase mOpenHelper;

    //Utility class to aid in matching URIs in content Provider
    private static final UriMatcher  uriMatcher = buildUriMatcher();

    /**
     * Authority is the name of the provider and must be unique
     * When another classes uses the ContentProvider they will actually use the URI
     */
    public static final String CONTENT_AUTHORITY = "com.example.tasktimer.provider";
    public static final Uri CONTENT_AUTHORITY_URI = Uri.parse("content://"+CONTENT_AUTHORITY);
    public static final int TASKS = 100;
    public static final int TASKS_ID = 101;

    private static final int TIMINGS = 200;
    public static final int TIMINGS_ID = 201;

    public static final int TASKS_DURATIONS = 400;
    public static final int TASKS_DURATION_ID = 401;
    //Uniform Resource Identifier

    /**
     * A content URI is a URI that identifies data in a provider. Content URIs include the symbolic name of the
     * entire provider (its authority) and a name that points to a table (a path). When you call a client method
     * to access a table in a provider, the content URI for the table is one of the arguments.
     * @return
     */
    public static UriMatcher buildUriMatcher(){

        final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        //If there is no table in the URI the match is going to return NO_MATCH

        //e.g. content://com.example.taskTimer.provider/Tasks
        uriMatcher.addURI(CONTENT_AUTHORITY, TasksContract.TABLE_NAME, TASKS);
        //e.g. content://com.example.taskTimer.provider/Tasks/8
        uriMatcher.addURI(CONTENT_AUTHORITY, TasksContract.TABLE_NAME+"/#", TASKS_ID);

        uriMatcher.addURI(CONTENT_AUTHORITY, TimingsContract.TABLE_NAME, TIMINGS);
        uriMatcher.addURI(CONTENT_AUTHORITY, TimingsContract.TABLE_NAME+"/#", TIMINGS_ID);

        return uriMatcher;
    }

    /**
     * Initialize your provider. The Android system calls this method immediately after it creates your provider.
     * Notice that your provider is not created until a ContentResolver object tries to access it.
     * @return
     */
    @Override
    public boolean onCreate() {
        mOpenHelper = AppDatabase.getInstance(getContext());
        return false;
    }

    /**
     * Retrieve data from your provider. Use the arguments to select the table to query, the rows and columns to return, and the sort
     * order of the result. Return the data as a Cursor object.
     * @param uri
     * @param projection
     * @param selection
     * @param selectionArgs
     * @param sortOrder
     * @return
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.d(TAG, "query: called with URI "+uri);
        final int matcher = uriMatcher.match(uri);
        Log.d(TAG, "query: the match code is "+matcher);

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        //convenience class that helps build SQLite queries to send to the SQLiteDatabase
        /**
         * We are using the switch block to select different blocks of code depending on the result of matching the Uri
         */
        switch (matcher){
            case TASKS:
                queryBuilder.setTables(TasksContract.TABLE_NAME);//If the Uri just contained the table name, then the basic query is just an SQL select statement
                break;
            case TASKS_ID://If id is included in the Uri then the match will return the TASKS_ID
                queryBuilder.setTables(TasksContract.TABLE_NAME);
                long taskId = TasksContract.getTaskId(uri);
                queryBuilder.appendWhere(TasksContract.Columns._ID +" = "+taskId);

            case TIMINGS:
                queryBuilder.setTables(TimingsContract.TABLE_NAME);
                break;
            case TIMINGS_ID:
                queryBuilder.setTables(TimingsContract.TABLE_NAME);
                long timingId = TimingsContract.getTimingId(uri);
                queryBuilder.appendWhere(TimingsContract.Columns._ID +" = "+timingId);

//            case TASKS_DURATIONS:
//                queryBuilder.setTables(DurationsContract.TABLE_NAME);
//                break;
//            case TASKS_DURATION_ID:
//                queryBuilder.setTables(DurationsContract.TABLE_NAME);
//                long taskDurationId = DurationsContract.getDurationId(uri);
//                queryBuilder.appendWhere(DurationsContract.Columns._ID +" = "+taskDurationId);

                default:
                    throw new IllegalStateException("Unknown URI "+uri);
        }

        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);//the contentprovider query method calls querybuilder query method
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    /**
     * Return the MIME type corresponding to a content URI. This method is described in more detail in the section Implementing content provider MIME types. This is
     * bascically for other systems to know what type of data can we get back of content provider
     * @param uri
     * @return
     */
    @Override
    public String getType(Uri uri) {
        final int match = uriMatcher.match(uri);

        switch (match){

            case TASKS:
                return TasksContract.CONTENT_TYPE;
            case TASKS_ID:
                return TasksContract.CONTENT_ITEM_TYPE;
            case TIMINGS:
                return TimingsContract.CONTENT_TYPE;
            case TIMINGS_ID:
                return TimingsContract.CONTENT_ITEM_TYPE;
                default:
                    throw new IllegalStateException("Unknown Uri "+uri);

        }
    }

    /**
     * Insert a new row into your provider. Use the arguments to select the destination table and to get the column values to use.
     * Return a content URI for the newly-inserted row.
     * @param uri
     * @param contentValues
     * @return
     */
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {

        Log.d(TAG, "insert: Entering insert called with "+uri);

        final int match = uriMatcher.match(uri);

        Log.d(TAG, "insert: The match returned is "+match);

        final SQLiteDatabase db;

        Uri returnUri;
        long recordId;

        switch (match){

            case TASKS:
                db = mOpenHelper.getWritableDatabase();
                // Insert the new row, returning the primary key value of the new row
                recordId = db.insert(TasksContract.TABLE_NAME, null, contentValues);
                if (recordId >= 0){//making sure it is not -1
                    returnUri = TasksContract.buildTaskUri(recordId);
                }else {
                    throw new android.database.SQLException("Failed to insert into "+TasksContract.TABLE_NAME);
                }
                break;
            case TIMINGS:
                db = mOpenHelper.getWritableDatabase();
                recordId = db.insert(TimingsContract.TABLE_NAME, null, contentValues);
                if (recordId >= 0){//making sure it is not -1
                    returnUri = TimingsContract.buildTimingUri(recordId);
                }else {
                    throw new android.database.SQLException("Failed to insert into "+TasksContract.TABLE_NAME);
                }
                break;
                default:
                    throw new IllegalStateException("Unknown Uri "+uri);

        }
        Log.d(TAG, "insert: returning with uri "+returnUri);

        if (recordId >= 0){
            Log.d(TAG, "insert: setting notify changed with "+uri);
            getContext().getContentResolver().notifyChange(uri, null);
        }else{
            Log.d(TAG, "insert: nothing inserted");
        }

        return returnUri;
    }

    /**
     * Delete rows from your provider. Use the arguments to select the table and the rows to delete. Return the number of rows deleted.
     * @param uri
     * @param s
     * @param strings
     * @return
     */
    @Override
    public int delete(Uri uri, String s, String[] strings) {
        Log.d(TAG, "delete: called with uri "+uri);
        final int match = uriMatcher.match(uri);
        Log.d(TAG, "delete match returned "+match);
        final SQLiteDatabase sqLiteDatabase;
        int count;
        switch (match){
            case TASKS:
                sqLiteDatabase = mOpenHelper.getWritableDatabase();
                count= sqLiteDatabase.delete(TasksContract.TABLE_NAME, s, strings);
                break;

            case TASKS_ID:
                sqLiteDatabase = mOpenHelper.getWritableDatabase();
                long taskId = TasksContract.getTaskId(uri);
                s = TasksContract.Columns._ID+" = "+taskId;

                if ((strings != null) && (strings.length > 0)){
                    s += "AND ( "+strings+" ) ";
                }
                count = sqLiteDatabase.delete(TasksContract.TABLE_NAME, s, strings);
                break;

            case TIMINGS:
                sqLiteDatabase = mOpenHelper.getWritableDatabase();
                count= sqLiteDatabase.delete(TimingsContract.TABLE_NAME, s, strings);
                break;

            case TIMINGS_ID:
                sqLiteDatabase = mOpenHelper.getWritableDatabase();
                long timingId = TimingsContract.getTimingId(uri);
                s = TasksContract.Columns._ID+" = "+timingId;

                if ((strings != null) && (strings.length > 0)){
                    s += "AND ( "+strings+" ) ";
                }
                count = sqLiteDatabase.delete(TimingsContract.TABLE_NAME, s, strings);
                break;

            default:
                throw new IllegalStateException("Unknown Uri "+uri);

        }
        Log.d(TAG, "update: returning "+count);
        if (count > 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }else{
            Log.d(TAG, "delete: nothing deleted");
        }
        return count;
    }

    /**
     * Update existing rows in your provider. Use the arguments to select the table and rows to update and to
     * get the updated column values. Return the number of rows updated.
     * @param uri
     * @param contentValues
     * @param s
     * @param strings
     * @return
     */
    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        Log.d(TAG, "update: called with uri "+uri);
        final int match = uriMatcher.match(uri);
        Log.d(TAG, "update: match returned "+match);
        final SQLiteDatabase sqLiteDatabase;
        int count;
        switch (match){
            case TASKS:
                sqLiteDatabase = mOpenHelper.getWritableDatabase();
                count= sqLiteDatabase.update(TasksContract.TABLE_NAME, contentValues, s, strings);
                break;

            case TASKS_ID:
                sqLiteDatabase = mOpenHelper.getWritableDatabase();
                long taskId = TasksContract.getTaskId(uri);
                s = TasksContract.Columns._ID+" = "+taskId;

                if ((strings != null) && (strings.length > 0)){
                    s += "AND ( "+strings+" ) ";
                }
                count = sqLiteDatabase.update(TasksContract.TABLE_NAME, contentValues, s, strings);
                break;

            case TIMINGS:
                sqLiteDatabase = mOpenHelper.getWritableDatabase();
                count= sqLiteDatabase.update(TimingsContract.TABLE_NAME, contentValues, s, strings);
                break;

            case TIMINGS_ID:
                sqLiteDatabase = mOpenHelper.getWritableDatabase();
                long timingId = TimingsContract.getTimingId(uri);
                s = TasksContract.Columns._ID+" = "+timingId;

                if ((strings != null) && (strings.length > 0)){
                    s += "AND ( "+strings+" ) ";
                }
                count = sqLiteDatabase.update(TimingsContract.TABLE_NAME, contentValues, s, strings);
                break;

                default:
                    throw new IllegalStateException("Unknown Uri "+uri);

        }
        Log.d(TAG, "update: returning "+count);

        if (count > 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }else{
            Log.d(TAG, "update: nothing deleted");
        }
        return count;
    }
    /**
     * This class has all the methods representing the operations that we may perform using the SQL codes in a database
     * getContext().getContentResolver().notifyChange(uri, null);
     *
     * which is called when such as at ContentProvider's insert(), update() and delete() calls, so the Cursor (has setNotificationUri()) will be notified.
     *
     * If you are using CursorAdapter, by default, CursorAdapter objects will get this notification issued by getContext().getContentResolver().notifyChange.
     */
}
