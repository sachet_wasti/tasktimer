package com.example.tasktimer;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;

import com.example.tasktimer.debug.TestData;
import com.example.tasktimer.debug.TestTiming;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLData;

public class MainActivity extends AppCompatActivity implements CursorRecyclerViewAdapter.OnTaskClickListener {

    private static final String TAG = "MainActivity";

    private boolean mTwoPanes = false;

    private static final String Occupy_Screen = "AddEditActivityFragment";

    private Timings mCurrentTiming = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (findViewById(R.id.task_details_container) != null){
            mTwoPanes = true;
        }

        if (savedInstanceState != null){
            Log.d(TAG, "onCreate: "+savedInstanceState.getSerializable(Timings.class.getSimpleName()));
            mCurrentTiming = (Timings) savedInstanceState.getSerializable(Timings.class.getSimpleName());
            setTimingText(mCurrentTiming);
        }else setTimingText(null);
//        String [] projection = {
//                                TasksContract.Columns._ID,
//                                TasksContract.Columns.TASK_NAME,
//                                TasksContract.Columns.TASK_DESCRIPTION,
//                                TasksContract.Columns.TASK_SORTORDER
//                                };
//        ContentResolver contentResolver = getContentResolver();
//        ContentValues values = new ContentValues();
//        values.put(TasksContract.Columns.TASK_NAME, "Study");
//        values.put(TasksContract.Columns.TASK_DESCRIPTION, "Study Content");
//        values.put(TasksContract.Columns.TASK_SORTORDER, 2);
//
//        String where = TasksContract.Columns.TASK_NAME+" = ?";
//        String [] selectionArgs = {"Study"};
//
//        int count = contentResolver.update(TasksContract.CONTENT_URI,
//                                            values,
//                                        where,
//                                        selectionArgs);

//        Log.d(TAG, "onCreate: count "+uri);

//        Uri uri = contentResolver.insert(TasksContract.CONTENT_URI, values);
//        Cursor cursor = contentResolver.query(TasksContract.CONTENT_URI,
//                projection,
//                null,
//                null,
//                TasksContract.Columns.TASK_SORTORDER);

//        if (cursor != null){
//            Log.d(TAG, "onCreate: number of rows "+cursor.getCount());
//            while (cursor.moveToNext()){
//                for(int i=0; i<cursor.getColumnCount(); i++){
//                    Log.d(TAG, "onCreate: name "+cursor.getColumnName(i)+" "+cursor.getString(i));
//                }
//                Log.d(TAG, "onCreate: =========================================");
//            }
//            if (cursor.getColumnCount() <= 1){
//                Toast.makeText(this, "Presently Empty----", Toast.LENGTH_LONG).show();
//            }
//            cursor.close();
//        }else {
//            Toast.makeText(this, "Presently Empty", Toast.LENGTH_LONG).show();
//        }

//        AppDatabase appDatabase = AppDatabase.getInstance(this);
//        final SQLiteDatabase sqLiteDatabase = appDatabase.getReadableDatabase();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        if (BuildConfig.DEBUG){
            MenuItem generate = menu.findItem(R.id.menumain_generateData);
            generate.setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.menumain_showDuration:
                break;
            case R.id.menuMain_addTask:
                taskEditRequest(null);
                break;
            case R.id.action_settings:
                break;
            case R.id.menumain_showabout:
                break;
            case R.id.menumain_generateData:
                TestData.generateTestData(getContentResolver());
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onEditClick(Task task) {
        taskEditRequest(task);
    }

    @Override
    public void onDeleteClick(Task task) {
        final Task task1 = task;
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you really want to delete "+task.getName())
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getContentResolver().delete(TasksContract.buildTaskUri(task1.getid()), null, null);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        Dialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onTaskLongClick(Task task) {
        Log.d(TAG, "onTaskLongClick: called");
        Log.d(TAG, "onTaskLongClick: clicked on "+task.getName());
        if (mCurrentTiming != null){
            if (task.getid() == mCurrentTiming.getMtask().getid()){
                setTimings(mCurrentTiming);
                mCurrentTiming = null;
                setTimingText(mCurrentTiming);
            }else{
                //a new task is being timed, stop oldone first
                setTimings(mCurrentTiming);
                mCurrentTiming = new Timings(task);
                setTimingText(mCurrentTiming);
            }
        }else{
            mCurrentTiming = new Timings(task);
            setTimingText(mCurrentTiming);
        }
    }

    private void setTimings(Timings currentTiming){
        Log.d(TAG, "setTimings: Entering saveTiming");
        //If there is open timing set duration and save
        currentTiming.setDuration();

        ContentResolver contentResolver = getContentResolver();
        ContentValues values = new ContentValues();
        values.put(TimingsContract.Columns.TIMING_TASK_ID, currentTiming.getMtask().getid());
        values.put(TimingsContract.Columns.TIMING_START_TIME, currentTiming.getStartTime());
        values.put(TimingsContract.Columns.TIMING_DURATION, currentTiming.getDuration());

        Uri uri = contentResolver.insert(TimingsContract.CONTENT_URI, values);

        Log.d(TAG, "setTimings: Exiting Timings contract with uri "+uri);

    }

    private void setTimingText(Timings currentTiming){

        TextView setTime = findViewById(R.id.current_task);
        if (currentTiming != null){
            setTime.setText("Timing "+currentTiming.getMtask().getName());
        }else {
            setTime.setText(R.string.no_task_selected);
        }

    }

    private void taskEditRequest(Task task){
        Log.d(TAG, "taskEditRequest: starts");
        if (mTwoPanes){
            Log.d(TAG, "taskEditRequest: in two panes mode");
            AddEditActivityFragment fragment = new AddEditActivityFragment();

            Bundle arguments = new Bundle();
            arguments.putSerializable(Task.class.getSimpleName(), task);
            fragment.setArguments(arguments);

            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.task_details_container, fragment);
            transaction.commit();

        }else {
            Log.d(TAG, "taskEditRequest: single-pane mode");
            //In single pane mode
            Intent detailIntent = new Intent(this, AddEditTask.class);
            if (task != null){
                detailIntent.putExtra(Task.class.getSimpleName(), task);
                startActivity(detailIntent);
            }else {//Adding new Task, as it is null
                startActivity(detailIntent);
            }
        }



    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        Log.d(TAG, "onSaveInstanceState: putting "+outState);
        outState.putSerializable(Timings.class.getSimpleName(), mCurrentTiming);
        super.onSaveInstanceState(outState);
    }
}
