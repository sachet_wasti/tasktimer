package com.example.tasktimer;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class AddEditActivityFragment extends Fragment {
    private static final String TAG = "AddEditActivityFragment";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FragmentEditMode mode;

    private EditText addedit_name;
    private EditText addedit_description;
    private EditText addedit_sortorder;
    private Button addedit_save;

    public enum FragmentEditMode {ADD, EDIT};


    public AddEditActivityFragment() {
        // Required empty public constructor
        Log.d(TAG, "AddEditActivityFragment: constructor called");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: starts");

        View view = inflater.inflate(R.layout.fragment_occupy_screen, container, false);

        addedit_name = view.findViewById(R.id.addedit_name);
        addedit_description = view.findViewById(R.id.addedit_description);
        addedit_sortorder = view.findViewById(R.id.addedit_sortorder);
        addedit_save = view.findViewById(R.id.addedit_save_text);

        Bundle arguments = getArguments();

        final Task task;

        if (arguments != null){
            task = (Task) arguments.getSerializable(Task.class.getSimpleName());
            if (task != null){
                addedit_name.setText(task.getName());
                addedit_description.setText(task.getDescription());
                addedit_sortorder.setText(Integer.toString(task.getSortOrder()));
                mode = FragmentEditMode.EDIT;
            }else {
                mode = FragmentEditMode.ADD;
            }
        }else{
            task = null;
            mode = FragmentEditMode.ADD;
        }

        addedit_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContentResolver contentResolver = getActivity().getContentResolver();
                ContentValues values = new ContentValues();

                int so;

                if (addedit_sortorder.length() > 0){
                    so = Integer.parseInt(addedit_sortorder.getText().toString());
                }else {
                    so = 0;
                }

                switch (mode){

                    case EDIT:
                        if (!addedit_name.getText().toString().equals(task.getName())){
                            values.put(TasksContract.Columns.TASK_NAME, addedit_name.getText().toString());
                        }
                        if (!addedit_description.getText().toString().equals(task.getDescription())){
                            values.put(TasksContract.Columns.TASK_DESCRIPTION, addedit_description.getText().toString());
                        }
                        if (so != task.getSortOrder()){
                            values.put(TasksContract.Columns.TASK_SORTORDER, so);
                        }if (values.size() > 0){
                        Log.d(TAG, "onClick: updating record");
                        contentResolver.update(TasksContract.buildTaskUri(task.getid()),
                                                values,
                                                null,
                                                null);
                    }
                        break;

                    case ADD:
                        if (addedit_name == null || addedit_description == null || addedit_name.getText().toString().equals("")
                           ||addedit_description.getText().toString().equals("")) {
                            Toast.makeText(getActivity(), "Enter something first", Toast.LENGTH_LONG).show();
                        }
                        if (addedit_name.length() > 0){
                            values.put(TasksContract.Columns.TASK_NAME, addedit_name.getText().toString());
                            values.put(TasksContract.Columns.TASK_DESCRIPTION, addedit_description.getText().toString());
                            values.put(TasksContract.Columns.TASK_SORTORDER, so);
                            contentResolver.insert(TasksContract.CONTENT_URI, values);
                        }
                    break;
                }

                addedit_name.setText("");
                addedit_description.setText("");
                addedit_sortorder.setText("");

            }
        });

        return view;
    }

}















