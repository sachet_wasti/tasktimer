package com.example.tasktimer;

import android.util.Log;

import java.io.Serializable;
import java.util.Date;

class Timings implements Serializable {
    private static final long serialVersionUID = 20190807L;
    private static final String TAG = Timings.class.getSimpleName();

    private long _id;
    private Task mtask;
    private long mStartTime;
    private long mDuration;

    public Timings(Task mtask) {
        this.mtask = mtask;
        //Initialise the startTime to now and duration to 0
        Date currentTime = new Date();
        mStartTime = currentTime.getTime()/1000;
        mDuration = 0;
    }

    public long get_id() {
        return _id;
    }

    public Task getMtask() {
        return mtask;
    }

    public long getStartTime() {
        return mStartTime;
    }

    public long getDuration() {
        return mDuration;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public void setMtask(Task mtask) {
        this.mtask = mtask;
    }

    public void setStartTime(long startTime) {
        mStartTime = startTime;
    }

    public void setDuration() {
        Date currentTime = new Date();
        mDuration = (currentTime.getTime() / 1000) - mStartTime;
        Log.d(TAG, "setDuration: --StartTime: "+mStartTime+" Duration "+mDuration);
    }
}
