package com.example.tasktimer;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

class CursorRecyclerViewAdapter extends RecyclerView.Adapter<CursorRecyclerViewAdapter.TaskViewHolder> {
    private static final String TAG = "CursorRecyclerViewAdapt";
    private Cursor mCursor;

    private OnTaskClickListener onTaskClickListener;

    interface OnTaskClickListener{
        void onEditClick(Task task);
        void onDeleteClick(Task task);
        void onTaskLongClick(Task task);
    }

    public CursorRecyclerViewAdapter(Cursor cursor, OnTaskClickListener onTaskClickListener) {
        Log.d(TAG, "CursorRecyclerViewAdapter: Constructor called");
        mCursor = cursor;
        this.onTaskClickListener = onTaskClickListener;
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: new view requested");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_list_items, parent, false);
        return new TaskViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: starts");

        if((mCursor == null) || (mCursor.getCount() == 0)) {
            Log.d(TAG, "onBindViewHolder: providing instructions");
            holder.name.setText("Instructions");
            holder.description.setText("Use the add button (+) in the toolbar above to create new tasks." +
                    "\n\nTasks with lower sort orders will be placed higher up the list." +
                    "Tasks with the same sort order will be sorted alphabetically." +
                    "\n\nTapping a task will start the timer for that task (and will stop the timer for any previous task that was being timed)." +
                    "\n\nEach task has Edit and Delete buttons if you eant to change the details or remove the task.");
            holder.editButton.setVisibility(View.GONE);
            holder.deleteButton.setVisibility(View.GONE);
        } else {
            if(!mCursor.moveToPosition(position)) {
                throw new IllegalStateException("Couldn't move cursor to position " + position);
            }

            final Task task = new Task(mCursor.getLong(mCursor.getColumnIndex(TasksContract.Columns._ID)),
                                        mCursor.getString(mCursor.getColumnIndex(TasksContract.Columns.TASK_NAME)),
                                        mCursor.getString(mCursor.getColumnIndex(TasksContract.Columns.TASK_DESCRIPTION)),
                                        mCursor.getInt(mCursor.getColumnIndex(TasksContract.Columns.TASK_SORTORDER)));

            holder.name.setText(task.getName());
            holder.description.setText(task.getDescription());
            holder.editButton.setVisibility(View.VISIBLE);  // TODO add onClick listener
            holder.deleteButton.setVisibility(View.VISIBLE); // TODO add onClick listener

            View.OnClickListener onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (view.getId()){
                        case R.id.tli_edit:
                            if (onTaskClickListener != null)onTaskClickListener.onEditClick(task);
                            break;
                        case R.id.tli_delete:
                            if (onTaskClickListener != null)onTaskClickListener.onDeleteClick(task);
                            break;
                            default:
                                Log.d(TAG, "onClick: unkown button ID");
                    }
                    Log.d(TAG, "onClick: The button with id "+view.getId()+" clicked");
                    Log.d(TAG, "onClick: The task "+task.getName()+" selected");
                }
            };

            View.OnLongClickListener buttonLongClick = new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    Log.d(TAG, "onLongClick: starts");
                    if (onTaskClickListener != null){
                        onTaskClickListener.onTaskLongClick(task);
                        return true;
                    }
                    return false;
                }
            };

            holder.editButton.setOnClickListener(onClickListener);
            holder.deleteButton.setOnClickListener(onClickListener);
            holder.itemView.setOnLongClickListener(buttonLongClick);
            holder.name.setOnLongClickListener(buttonLongClick);
            holder.description.setOnLongClickListener(buttonLongClick);
        }

    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: starts");
        if((mCursor == null) || (mCursor.getCount() == 0)) {
            Log.d(TAG, "getItemCount: returning "+1);
            return 1; // fib, because we populate a single ViewHolder with instructions
        } else {
            Log.d(TAG, "getItemCount: returning "+mCursor.getColumnCount());
            return mCursor.getCount();
        }
    }

    /**
     * Swap in new Cursor returning the old cursor
     * The returned ld cursor is not closed
     * @param newCursor
     * @return
     */
    Cursor swapCursor(Cursor newCursor){

        Log.d(TAG, "swapCursor: called "+newCursor);

        if (newCursor == mCursor){
            return null;
        }

        final Cursor oldCursor = mCursor;
        mCursor = newCursor;
        if (newCursor != null){
            notifyDataSetChanged();
        }else{
            notifyItemRangeRemoved(0, mCursor.getColumnCount());
        }

        return oldCursor;

    }

    static class TaskViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "TaskViewHolder";

        TextView name = null;
        TextView description = null;
        ImageButton editButton = null;
        ImageButton deleteButton = null;
        View itemView;

        public TaskViewHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "TaskViewHolder: starts");

            this.name = (TextView) itemView.findViewById(R.id.tli_name);
            this.description = (TextView) itemView.findViewById(R.id.tli_description);
            this.editButton = (ImageButton) itemView.findViewById(R.id.tli_edit);
            this.deleteButton = (ImageButton) itemView.findViewById(R.id.tli_delete);
            this.itemView = itemView;
        }
    }
}
