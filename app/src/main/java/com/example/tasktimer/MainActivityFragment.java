package com.example.tasktimer;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.security.InvalidParameterException;


public class MainActivityFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = "MainActivityFragment";
    
    public static final int LOADER_ID = 0;

    private CursorRecyclerViewAdapter mAdapter;//add adapter refrence
    
    public MainActivityFragment() {
        Log.d(TAG, "MainActivityFragment: constructor created");
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LoaderManager.getInstance(this).initLoader(LOADER_ID, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_activity, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.task_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mAdapter = new CursorRecyclerViewAdapter(null, (CursorRecyclerViewAdapter.OnTaskClickListener)getActivity());
        recyclerView.setAdapter(mAdapter);

        return view;
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        Log.d(TAG, "onCreateLoader: starts with id "+id);
        String[] projection = {TasksContract.Columns._ID, TasksContract.Columns.TASK_NAME,
                                TasksContract.Columns.TASK_DESCRIPTION, TasksContract.Columns.TASK_SORTORDER};
        String sortOrder = TasksContract.Columns.TASK_SORTORDER+","+TasksContract.Columns.TASK_NAME+" COLLATE NOCASE";

        switch (id){
             case LOADER_ID:
                 return new CursorLoader(getActivity(),
                                        TasksContract.CONTENT_URI,
                                        projection,
                                        null,
                                        null,
                                        sortOrder);
             default:
                 throw new InvalidParameterException(TAG+"oncreateloader called with invalid parameter"+id);
         }
    }

    /**
     * Method called after cursorLoader has finished loading data on a background thread
     * @param loader
     * @param data
     */
    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {

        Log.d(TAG, "onLoadFinished: Entered onLoadFinished");
        mAdapter.swapCursor(data);
        int count = mAdapter.getItemCount();


    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        Log.d(TAG, "onLoaderReset: ");
        mAdapter.swapCursor(null);
    }
}
