package com.example.tasktimer;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

import static com.example.tasktimer.AppProvider.CONTENT_AUTHORITY;
import static com.example.tasktimer.AppProvider.CONTENT_AUTHORITY_URI;

public class TimingsContract {
    //Content provider abstracts the underlying data and prvents the statndered interface that we use to access the data
    //We are going to provide constants for accessing the data
    static final String TABLE_NAME = "Timings";

    //Timings fields
    public static class Columns{
        public static final String _ID = BaseColumns._ID;
        public static final String TIMING_TASK_ID = "TaskId";
        public static final String TIMING_START_TIME = "StartTime";
        public static final String TIMING_DURATION = "Duration";
        private Columns(){
            //private constructor to prevent initialisation
        }
    }
    //The Columns class behaves almost exactly the same as it were defined in its own separate .java class, we cannot refer it directly using Columns
    /**
     * URI to access the timings table, it can be used by different applications or classes that uses our content provider to access the TIMINGS_TABLE
     * A content URI is a URI that identifies data in a provider. Content URIs include the symbolic name of the entire provider (its authority)
     * and a name that points to a table (a path). When you call a client method to access a table in a provider, the content URI for the table
     * is one of the arguments.
     *
     * In the preceding lines of code, the constant CONTENT_URI contains the content URI of the user dictionary's "words" table. The ContentResolver
     * object parses out the URI's authority, and uses it to "resolve" the provider by comparing the authority to a system table of known providers.
     * The ContentResolver can then dispatch the query arguments to the correct provider.
     *
     * The ContentProvider uses the path part of the content URI to choose the table to access. A provider usually has a path for each table it exposes.
     *
     * content://user_dictionary/words
     *
     * where the user_dictionary string is the provider's authority, and the words string is the table's path.
     * The string content:// (the scheme) is always present, and identifies this as a content URI.
     */
    public static final Uri CONTENT_URI =  Uri.withAppendedPath(CONTENT_AUTHORITY_URI, TABLE_NAME);

    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd."+CONTENT_AUTHORITY+"."+TABLE_NAME;
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd."+CONTENT_AUTHORITY+"."+TABLE_NAME;

    public static long getTimingId(Uri uri){
        return ContentUris.parseId(uri);
    }

    public static Uri buildTimingUri(long timingId){
        return ContentUris.withAppendedId(CONTENT_URI, timingId);
    }

}
