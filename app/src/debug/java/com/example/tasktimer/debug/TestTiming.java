package com.example.tasktimer.debug;

public class TestTiming {

    long taskId;
    long starttime;
    long duration;

    public TestTiming(long taskId, long starttime, long duration) {
        this.taskId = taskId;
        this.starttime = starttime;
        this.duration = duration;
    }
}
