package com.example.tasktimer.debug;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.example.tasktimer.TasksContract;
import com.example.tasktimer.TimingsContract;

import java.util.GregorianCalendar;

public class TestData {

    public static void generateTestData(ContentResolver contentResolver){

        final int SEC_IN_DAY = 86400;
        final int LOWER_BOUND = 100;
        final int UPPER_BOUND = 500;
        final int MAX_DURATION = SEC_IN_DAY / 6;

        String [] projection = {TasksContract.Columns._ID};

        Uri uri = TasksContract.CONTENT_URI;
        Cursor cursor = contentResolver.query(uri, projection, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {

                long taskId = cursor.getLong(cursor.getColumnIndex(TasksContract.Columns._ID));

                long loopCount = LOWER_BOUND + getRandomData(UPPER_BOUND - LOWER_BOUND);

                for (int i=0; i< loopCount; i++){

                    long randomDate = randomDateTime();

                    long duration = (long) getRandomData(MAX_DURATION);

                    //create a new testTimingObject
                    TestTiming testTiming = new TestTiming(taskId, randomDate, duration);

                    //add it to database
                    saveCurrentTiming(contentResolver, testTiming);

                }
            } while (cursor.moveToNext());
        }

    }

    private static int getRandomData(int max){
        return (int)Math.round(Math.random() * max);
    }

    private static long randomDateTime(){
        final int startyear = 2017;
        final int endYear = 2018;

        int sec = getRandomData(59);
        int min = getRandomData(59);
        int hour = getRandomData(23);
        int month = getRandomData(11);
        int year = startyear + getRandomData(endYear-startyear);

        GregorianCalendar gc = new GregorianCalendar(year, month, 1);
        int day = 1 + getRandomData(gc.getActualMaximum(GregorianCalendar.DAY_OF_MONTH)-1);

        gc.set(year, month, day, hour, min, sec);
        return gc.getTimeInMillis();
    }

    private static void saveCurrentTiming(ContentResolver contentResolver, TestTiming testTiming){

        ContentValues contentValues = new ContentValues();
        contentValues.put(TimingsContract.Columns.TIMING_TASK_ID, testTiming.taskId);
        contentValues.put(TimingsContract.Columns.TIMING_START_TIME, testTiming.starttime);
        contentValues.put(TimingsContract.Columns.TIMING_DURATION, testTiming.duration);

        contentResolver.insert(TimingsContract.CONTENT_URI, contentValues);

    }

}









